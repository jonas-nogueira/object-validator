const { enumValidator } = require('../src/validators');
const validateSchema = require('../src/validateSchema');

describe('Testing enumValidator', () => {
  it('should not return error when using a valid enum value', () => {
    const schema = [
      ['gender', true, [enumValidator(['male', 'female'])]],
      ['status', true, [enumValidator(['opened', 'closed'])]]
    ];

    const data = {
      gender: 'male',
      status: 'closed'
    };

    const errors = validateSchema(data, schema);
    expect(errors).toEqual([]);
  });

  it('should returns errors when using a invalid enum value', () => {
    const schema = [
      ['gender', true, [enumValidator(['male', 'female'])]],
      ['status', true, [enumValidator(['opened', 'closed'])]]
    ];

    const data = {
      gender: 'other',
      status: 'none'
    };

    const errors = validateSchema(data, schema);
    expect(errors.length).toEqual(2);
  });
});
