const validateSchema = require('../src/validateSchema');

describe('Testing required and optional fields', () => {
  it('should not return errors when validating a plain object that contains all required fields and no optional fields ', () => {
    const schema = [['name', true, []], ['email', true, []]];

    const data = {
      name: 'jonas',
      email: 'jonas@gmail.com'
    };

    const errors = validateSchema(data, schema);
    expect(errors).toEqual([]);
  });

  it('should return error when validating a plain object that does not contain all required fields', () => {
    const schema = [['name', true, []], ['email', true, []]];

    const data = {
      name: 'jonas'
    };

    const errors = validateSchema(data, schema);
    expect(errors.length).toBe(1);
  });

  it('should not return errors when validating a plain object that contains all required fields, but does not contain the optional fields', () => {
    const schema = [['name', true, []], ['email', false, []]];

    const data = {
      name: 'jonas'
    };

    const errors = validateSchema(data, schema);
    expect(errors).toEqual([]);
  });

  it('should not return errors when validating a nested object that contains all required fields and no optional fields ', () => {
    const schema = [['address.street', true, []], ['email', true, []]];

    const data = {
      address: {
        street: 'rua a'
      },
      email: 'jonas@gmail.com'
    };

    const errors = validateSchema(data, schema);
    expect(errors).toEqual([]);
  });

  it('should return error when validating a nested object that does not contain all required fields', () => {
    const schema = [['address.street', true, []], ['email', true, []]];

    const data = {
      name: 'jonas'
    };

    const errors = validateSchema(data, schema);
    expect(errors.length).toBe(2);
  });
});
