const { maxLength } = require('../src/validators');
const validateSchema = require('../src/validateSchema');

describe('Testing maxLengthValidator', () => {
  it('should not returns errors when validating a string with length < validator parameter', () => {
    const schema = [
      ['name', true, [maxLength(10)]],
      ['email', true, [maxLength(20)]]
    ];

    const data = {
      name: 'jonas',
      email: 'jonas@gmail.com'
    };

    const errors = validateSchema(data, schema);
    expect(errors).toEqual([]);
  });

  it('should not returns errors when validating a string with length = validator parameter', () => {
    const schema = [
      ['name', true, [maxLength(5)]],
      ['email', true, [maxLength(15)]]
    ];

    const data = {
      name: 'jonas',
      email: 'jonas@gmail.com'
    };

    const errors = validateSchema(data, schema);
    expect(errors).toEqual([]);
  });

  it('should returns errors when validating a string with length > validator parameter', () => {
    const schema = [
      ['name', true, [maxLength(4)]],
      ['email', true, [maxLength(4)]]
    ];

    const data = {
      name: 'jonas',
      email: 'jonas@gmail.com'
    };

    const errors = validateSchema(data, schema);
    expect(errors.length).toBe(2);
  });

  it('should not returns errors when validating a string, in nested object, with length < validator parameter', () => {
    const schema = [
      ['address.street', true, [maxLength(10)]],
      ['user.name', true, [maxLength(10)]]
    ];

    const data = {
      user: { name: 'jonas' },
      address: {
        street: 'rua a'
      }
    };

    const errors = validateSchema(data, schema);
    expect(errors).toEqual([]);
  });

  it('should not returns errors when validating a string, in nested object, with length = validator parameter', () => {
    const schema = [
      ['address.street', true, [maxLength(5)]],
      ['user.name', true, [maxLength(5)]]
    ];

    const data = {
      user: { name: 'jonas' },
      address: {
        street: 'rua a'
      }
    };

    const errors = validateSchema(data, schema);
    expect(errors).toEqual([]);
  });

  it('should returns errors when validating a string in nested object with length > validator parameter', () => {
    const schema = [
      ['address.street', true, [maxLength(2)]],
      ['user.name', true, [maxLength(2)]]
    ];

    const data = {
      user: { name: 'jonas' },
      address: {
        street: 'rua a'
      }
    };

    const errors = validateSchema(data, schema);
    expect(errors.length).toBe(2);
  });
});
