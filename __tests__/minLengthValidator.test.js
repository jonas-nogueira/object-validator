const { minLength } = require('../src/validators');
const validateSchema = require('../src/validateSchema');

describe('Testing minLengthValidator', () => {
  it('should not returns errors when validating a string with length > validator parameter', () => {
    const schema = [
      ['name', true, [minLength(2)]],
      ['email', true, [minLength(10)]]
    ];

    const data = {
      name: 'jonas',
      email: 'jonas@gmail.com'
    };

    const errors = validateSchema(data, schema);
    expect(errors).toEqual([]);
  });

  it('should not returns errors when validating a string with length = validator parameter', () => {
    const schema = [
      ['name', true, [minLength(5)]],
      ['email', true, [minLength(15)]]
    ];

    const data = {
      name: 'jonas',
      email: 'jonas@gmail.com'
    };

    const errors = validateSchema(data, schema);
    expect(errors).toEqual([]);
  });

  it('should returns errors when validating a string with length < validator parameter', () => {
    const schema = [
      ['name', true, [minLength(6)]],
      ['email', true, [minLength(16)]]
    ];

    const data = {
      name: 'jonas',
      email: 'jonas@gmail.com'
    };

    const errors = validateSchema(data, schema);
    expect(errors.length).toBe(2);
  });

  it('should not returns errors when validating a string, in nested object, with length > validator parameter', () => {
    const schema = [
      ['address.street', true, [minLength(2)]],
      ['user.name', true, [minLength(2)]]
    ];

    const data = {
      user: { name: 'jonas' },
      address: {
        street: 'rua a'
      }
    };

    const errors = validateSchema(data, schema);
    expect(errors).toEqual([]);
  });

  it('should not returns errors when validating a string, in nested object, with length = validator parameter', () => {
    const schema = [
      ['address.street', true, [minLength(5)]],
      ['user.name', true, [minLength(5)]]
    ];

    const data = {
      user: { name: 'jonas' },
      address: {
        street: 'rua a'
      }
    };

    const errors = validateSchema(data, schema);
    expect(errors).toEqual([]);
  });

  it('should returns errors when validating a string in nested object with length < validator parameter', () => {
    const schema = [
      ['address.street', true, [minLength(6)]],
      ['user.name', true, [minLength(10)]]
    ];

    const data = {
      user: { name: 'jonas' },
      address: {
        street: 'rua a'
      }
    };

    const errors = validateSchema(data, schema);
    expect(errors.length).toBe(2);
  });
});
