const schema = [
  [
    'propertyName: string',
    'required: boolean',
    'validatorList: Array<Validator>'
  ]
];

const Validator = 'fn () => fn (fieldName, fieldValue) => error | undefined';

module.exports = { schema, Validator };
