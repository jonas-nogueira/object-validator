const R = require('ramda');

function validateField (value, [propertyName, validatorList]) {
  return validatorList
    .map(validator => validator(propertyName, value))
    .filter(value => value !== undefined);
}

function getProperty (propertyName, object) {
  const propertyNameList = R.split('.', propertyName);
  if (propertyNameList.length === 1) {
    return R.prop(propertyName, object);
  }
  return propertyNameList.reduce((property, currentPropertyName) => {
    if (!property) {
      return R.prop(currentPropertyName, object);
    }
    return R.prop(currentPropertyName, property);
  }, undefined);
}

function validateSchema (dataToValidate, schema) {
  return schema
    .map(([propertyName, required, validatorsList]) => {
      const value = getProperty(propertyName, dataToValidate);
      if (required && !value) {
        return `${propertyName} não informado.`;
      } else if (!value) {
        return [];
      }
      return validateField(value, [propertyName, validatorsList]);
    })
    .filter(value => value.length > 0);
}

module.exports = validateSchema;
