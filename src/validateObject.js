function validateObject (sanitizedObject, schema, validators) {
  function filterUndefined (value) {
    return value !== undefined;
  }

  const errors = Object.entries(schema).map(([key, value]) => {
    if (typeof value !== 'object') {
      throw new Error('invalid schema');
    }
    if (Array.isArray(value)) {
      throw new Error('invalid schema');
    }

    const fieldErrors = Object.keys(value).map(valueKey => {
      if (validators[valueKey]) {
        return validators[valueKey](sanitizedObject[key], value[valueKey]);
      } else {
        const nestedSchema = {};
        nestedSchema[valueKey] = schema[key][valueKey];
        fieldErrors[valueKey] = validateObject(
          sanitizedObject[key][valueKey],
          nestedSchema,
          validators
        );
      }
    });

    const validErrors = fieldErrors.filter(filterUndefined);
    if (validErrors.length === 0) {
      return undefined;
    }

    return [key, validErrors];
  });

  return errors.filter(filterUndefined).reduce((errorsObject, [key, value]) => {
    errorsObject[key] = value;
    return errorsObject;
  }, {});
}

module.exports = validateObject;
