function minLengthValidator (value, options) {
  const { message } = options || {};
  return (fieldName, fieldValue) => {
    if (fieldValue.length < value) {
      return message || `${fieldName} deve ter ao menos ${value} caracteres`;
    }
  };
}

module.exports = minLengthValidator;
