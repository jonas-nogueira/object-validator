function maxLengthValidator (value, options) {
  const { message } = options || {};
  return (fieldName, fieldValue) => {
    if (fieldValue.length > value) {
      return message || `${fieldName} deve ter no maximo ${value} caracteres`;
    }
  };
}

module.exports = maxLengthValidator;
