function enumValidator (values, options) {
  const { message } = options || {};
  return (fieldName, fieldValue) => {
    if (!values.includes(fieldValue)) {
      return (
        message ||
        `O valor informado para ${fieldName} é invalido. Informe desses valores ${values}`
      );
    }
  };
}

module.exports = enumValidator;
